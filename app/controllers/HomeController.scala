package controllers

import javax.inject._

import akka.util.ByteString
import play.api._
import play.api.http.HttpEntity
import play.api.mvc._

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class HomeController @Inject()(/*config: Configuration ,*/ cc: ControllerComponents) extends AbstractController(cc) {

  /**
   * Create an Action to render an HTML page.
   *
   * The configuration in the `routes` file means that this method
   * will be called when the application receives a `GET` request with
   * a path of `/`.
   */
  def index() = Action { implicit request: Request[AnyContent] =>

    Ok(views.html.index())
//    Ok(config.get[String]("play.editor"))
    // anotherMethod("Invvoque")
   // Ok("Got request [" + request + "]")

  }

  // ejemplo estados HTTP
  def badRequest() = Action {
    /*
    Result(
      header = ResponseHeader(400, Map.empty),
      body = HttpEntity.Strict(ByteString("Bad request!"), Some("text/plain"))
    )
    */
    //val ok = Ok("Hello world!")
    //val notFound = NotFound
    // val pageNotFound = NotFound(<h1>Page not found</h1>)
    //val badRequest = BadRequest(views.html.form(formWithErrors))
    //val oops = InternalServerError("Oops")
    Status(488)("Strange response type")
  }

  // ejemplo redireccionar
  def redirecciona() = Action {
    Redirect("http://facebook.com")
  }

  def notimplemented () = TODO

  def anotherMethod(p: String)(implicit request: Request[_]) = {
    // do something that needs access to the request
    println("anotherMethod say: " + request)
  }
}
